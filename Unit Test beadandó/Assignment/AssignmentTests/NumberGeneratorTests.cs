﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assignment.Numbers;

namespace AssignmentTests
{

    [TestFixture]
    public class NumberGeneratorTests
    {
        NumberGenerator numberGenerator;

        [SetUp]
        public void Setup()
        {
            numberGenerator = new NumberGenerator();
        }

        
        [TestCase(-14447)]
        [TestCase(-1)]
        [TestCase(-47)]
        [TestCase(-35)]
        public void GenerateEven_Should_ThrowArgumentOutOfRangeException_When_InputIsNegative(int input)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateEven(input));
        }

        [TestCase(-14447)]
        [TestCase(-1)]
        [TestCase(-47)]
        [TestCase(0)]
        public void GenerateOdd_Should_ThrowArgumentOutOfRangeException_When_InputIsLesserThanOne(int input)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateOdd(input));
        }

        [TestCase(4677)]
        [TestCase(14)]
        [TestCase(0)]
        [TestCase(1)]
        public void GenerateEven_Should_ReturnAnEvenNumberBetweenZeroAndLimit__When_InputIsGreaterThanOrEqualToZero(int limit)
        {
            int result = numberGenerator.GenerateEven(limit);
            Assert.IsTrue(result >= 0 && result <= limit);
            Assert.That(result % 2, Is.Zero); 
        }

        [TestCase(1478)]
        [TestCase(65)]
        [TestCase(6)]
        [TestCase(1)]
        public void GenerateOdd_Should_ReturnAnOddNumberBetweenOneAndLimit__When_InputIsGreaterThanOrEqualToOne(int limit)
        {
            int result = numberGenerator.GenerateOdd(limit);
            Assert.IsTrue(result >= 1 && result <= limit);
            Assert.That(result % 2, Is.EqualTo(1));

        }
    }
}

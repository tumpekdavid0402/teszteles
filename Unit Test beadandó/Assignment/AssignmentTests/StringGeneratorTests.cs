﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assignment.Numbers;
using Moq;
using Assignment.Strings;

namespace AssignmentTests
{
    [TestFixture]
    class StringGeneratorTests
    {
        Mock<INumberGenerator> numberGenerator;
        StringGenerator stringGenerator;
        int max = 5;
        int even = 2;
        int odd = 3;
        [SetUp]
        public void Setup()
        {
            numberGenerator = new Mock<INumberGenerator>();
            stringGenerator = new StringGenerator(numberGenerator.Object);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(4)]
        [TestCase(35)]
        [TestCase(0)]
        public void GenerateEvenOddPairs_Should_ReturnAListWithCountOfPairCount_When_AnyNotNegativeIntegerGivenAsPairCountInput(int pairCount)
        {
            numberGenerator.Setup(x => x.GenerateEven(max)).Returns(even);
            numberGenerator.Setup(x => x.GenerateOdd(max)).Returns(odd);
            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);
            Assert.AreEqual(pairCount, pairs.Count);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(4)]
        [TestCase(35)]
        [TestCase(0)]
        public void GenerateEvenOddPairs_Should_CallNumberGeneratorsGenerateEvenMethodPairCountTimes_When_AnyNotNegativeIntegerGivenAsPairCountInput(int pairCount)
        {
            numberGenerator.Setup(x => x.GenerateEven(max)).Returns(even);
            numberGenerator.Setup(x => x.GenerateOdd(max)).Returns(odd);
            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);
            numberGenerator.Verify(x => x.GenerateEven(max), Times.Exactly(pairCount));
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(4)]
        [TestCase(35)]
        [TestCase(0)]
        public void GenerateEvenOddPairs_Should_CallNumberGeneratorsGenerateOddMethodPairCountTimes_When_AnyNotNegativeIntegerGivenAsPairCountInput(int pairCount)
        {
            numberGenerator.Setup(x => x.GenerateEven(max)).Returns(even);
            numberGenerator.Setup(x => x.GenerateOdd(max)).Returns(odd);
            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);
            numberGenerator.Verify(x => x.GenerateOdd(max), Times.Exactly(pairCount));
        }


        [TestCase(-1)]
        [TestCase(-55)]
        [TestCase(-47)]
        [TestCase(-35)]
        [TestCase(-2)]
        public void GenerateEvenOddPairs_Should_ReturnAnEmptyList_When_AnyNegativeIntegerGivenAsPairCountInput(int pairCount)
        {
            numberGenerator.Setup(x => x.GenerateEven(max)).Returns(even);
            numberGenerator.Setup(x => x.GenerateOdd(max)).Returns(odd);
            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);
            Assert.AreEqual(0, pairs.Count);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(4)]
        public void GenerateEvenOddPairs_Should_ReturnAListWithElementsOfEvenOddPairs_When_AnyNotNegativeIntegerGivenAsPairCountInput(int pairCount)
        {
            numberGenerator.Setup(x => x.GenerateEven(max)).Returns(even);
            numberGenerator.Setup(x => x.GenerateOdd(max)).Returns(odd);
            string expected = even + "," + odd;
            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);
            pairs.ForEach(p => Assert.AreEqual(expected, p));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assignment.Strings;

namespace AssignmentTests
{
    [TestFixture]
    class StringUtilTests
    {

        StringUtil stringUtil;

        [SetUp]
        public void Setup()
        {
            stringUtil = new StringUtil();
        }

        [TestCase("valami")]
        [TestCase("nem")]
        [TestCase("palindrom")]
        [TestCase("teszteles")]
        public void IsPalindrom_ShouldReturnFalse_When_InputIsNotAPalindrome(string input)
        {
            Assert.IsFalse(stringUtil.IsPalindrom(input));
        }


        [TestCase("ama")]
        [TestCase("abba")]
        [TestCase("görög")]
        [TestCase("keretkarakterek")]

        public void IsPalindrom_ShouldReturnTrue_When_InputIsAPalindrome(string input)
        {
            Assert.IsTrue(stringUtil.IsPalindrom(input));
        }
    }
}

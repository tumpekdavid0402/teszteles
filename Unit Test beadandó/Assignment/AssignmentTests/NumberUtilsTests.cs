﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assignment.Numbers;

namespace AssignmentTests
{
    [TestFixture]
    class NumberUtilsTests
    {
        NumberUtils numberUtils;
        [SetUp]
        public void Setup()
        {
            numberUtils = new NumberUtils();
        }

        [TestCase(16)]
        [TestCase(22)]
        [TestCase(7)]
        [TestCase(100)]
        [TestCase(2)]
        [TestCase(-421)]
        [TestCase(-7)]
        public void GetDivisors_Should_ReturnAListAtLeastOfTwoNumbers_When_InputIsNotZeroOrOne(int input)
        {
            var divisors = numberUtils.GetDivisors(input);
            Assert.That(divisors.Count, Is.GreaterThanOrEqualTo(2));
        }

        [TestCase(16)]
        [TestCase(20)]
        [TestCase(7)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(100)]
        [TestCase(-1)]
        [TestCase(-421)]
        public void GetDivisors_Should_ReturnAListOfNumbersWithoutDuplicates_When_InputIsNotZero(int input)
        {
            var divisors = numberUtils.GetDivisors(input);
            var duplicates = divisors.GroupBy(d => d).Where(g => g.Count() > 1).ToList();
            Assert.That(duplicates.Count, Is.EqualTo(0));
        }

        [Test]
        public void Get_Divisors_Should_ReturnAListContainsOnlyNumberOne_When_InputIsOne()
        {
            var divisors = numberUtils.GetDivisors(1);
            Assert.That(divisors.Count, Is.EqualTo(1));
            Assert.That(divisors.First(), Is.EqualTo(1));
        }

        [TestCase(16)]
        [TestCase(22)]
        [TestCase(7)]
        [TestCase(20)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(-35)]
        [TestCase(-25)]
        [TestCase(-1243)]
        public void GetDivisors_Should_ReturnAListOfTheDivisorsOfTheInputNumbers_When_InputIsNotZero(int input)
        {
            var divisors = numberUtils.GetDivisors(input);
            divisors.ForEach(divisor => Assert.That(input % divisor == 0));
        }

        [Test]
        public void GetDivisors_Should_ThrowArgumentException_When_InputIsZero()
        {
            Assert.Throws(typeof(ArgumentException), () => numberUtils.GetDivisors(0));
        }


        [TestCase(2)]
        [TestCase(3)]
        [TestCase(7)]
        [TestCase(11)]
        [TestCase(23)]
        [TestCase(167)]
        [TestCase(199)]
        public void IsPrime_Should_ReturnTrue_When_InputIsPime(int input)
        {
            Assert.IsTrue(numberUtils.IsPrime(input));
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(4)]
        [TestCase(100)]
        [TestCase(24)]
        [TestCase(155)]
        [TestCase(-28)]
        [TestCase(-265)]
        public void IsPrime_Should_ReturnFalse_When_InputIsNotPrime(int input)
        {
            Assert.IsFalse(numberUtils.IsPrime(input));
        }

        [TestCase(0)] // szerintem a 0 páros
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(100)]
        [TestCase(-17898)]
        [TestCase(-2)]
        public void EvenOrOdd_Should_ReturnEven_When_InputIsEven(int input)
        {
            string res = numberUtils.EvenOrOdd(input);
            Assert.That(res, Is.EqualTo("even"));
        }

        [TestCase(1)] 
        [TestCase(7)]
        [TestCase(171)]
        [TestCase(-123)]
        [TestCase(-1789)]
        public void EvenOrOdd_Should_ReturnOdd_When_InputIsOdd(int input)
        {
            string res = numberUtils.EvenOrOdd(input);
            Assert.That(res, Is.EqualTo("odd"));
        }
    }
}

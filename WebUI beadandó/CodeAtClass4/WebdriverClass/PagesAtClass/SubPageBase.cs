﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public abstract class SubPageBase : BasePage
    {
        public abstract IWebElement Element { get; }
        public WebDriverWait WebDriverWait { get; }

        public SubPageBase(IWebDriver webDriver) : base(webDriver)
        {
            WebDriverWait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(5));
        }



    }
}

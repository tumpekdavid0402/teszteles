﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    class PickUpPointsPage : SubPageBase
    {
        public override IWebElement Element => WebDriverWait.Until(d => Driver.FindElement(By.CssSelector("img[src='http://konyvbagoly.hu/img/tmp/store_1.jpg']")));

        public PickUpPointsPage(IWebDriver webDriver) : base(webDriver)
        {
        }
    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public enum SubPageType
    {
        Login, PickUp, Help
    }
    class MainPage : BasePage
    {
        IWebElement LoginLink => Driver.FindElement(By.XPath("//div[@class='header_user_info clearfix not-logged-in']/a[@title='Bejelentkezés a vásárlói fiókba']"));
        IWebElement PickUpPointsLink => Driver.FindElement(By.CssSelector("a[href='http://konyvbagoly.hu/content/9-atveteli-pontok']"));
        IWebElement HelpToOrderLink => Driver.FindElement(By.CssSelector("a[title='Segítség az online rendeléshez']"));

        public MainPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static MainPage Navigate(IWebDriver webDriver)
        {

            webDriver.Navigate().GoToUrl("http://konyvbagoly.hu/");
            return new MainPage(webDriver);
        }

        /*
         * azért használtam ezt a javascriptes megoldást mert a pickup és a help esetében
         * mielőtt a clicket végrehajtotta volna a Selenium valamiért "ugrott" egyet az oldal és
         * ElementClickInterceptedException-t dobott (emiatt az ugrás miatt máshol landolt volna a click)
         * próbálkoztam több mindennel implicit wait-tel explicit waittel, az ablak maximalizálásával,
         * az Actions osztály használatával
         * de egyik másik megoldás sem bizonyult stabilnak csak ez
         */
        public SubPageBase GoToSubPage(SubPageType subPage)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            switch (subPage)
            {
                case SubPageType.Login:
                    LoginLink.Click();
                    return new LoginPage(Driver);
                case SubPageType.PickUp:
                    js.ExecuteScript("window.scrollTo(0, "+PickUpPointsLink.Location.Y+")");
                    PickUpPointsLink.Click();
                    return new PickUpPointsPage(Driver);
                case SubPageType.Help:
                    js.ExecuteScript("window.scrollTo(0, " + PickUpPointsLink.Location.Y + ")");
                    HelpToOrderLink.Click();
                    return new HelpToOrderPage(Driver);
            }
            throw new NotSupportedException();
        }

    }
}

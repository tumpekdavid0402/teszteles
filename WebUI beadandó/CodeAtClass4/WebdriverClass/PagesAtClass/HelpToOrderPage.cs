﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    class HelpToOrderPage : SubPageBase
    {
        public HelpToOrderPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override IWebElement Element => WebDriverWait.Until(d =>
                                                    Driver.FindElement(By.CssSelector("img[src='http://konyvbagoly.hu/img/cms/tutorial_photos/01border.jpg']")));
    }
}

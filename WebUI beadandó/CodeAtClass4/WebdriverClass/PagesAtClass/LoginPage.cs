﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    class LoginPage : SubPageBase
    {
        public LoginPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override IWebElement Element => WebDriverWait.Until(d => Driver.FindElement(By.Id("create-account_form")));
    }
}

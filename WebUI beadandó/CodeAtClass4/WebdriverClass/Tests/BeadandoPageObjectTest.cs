﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Tests
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource(nameof(GetAllSubPages))]
        public void BeadandoTest(SubPageType subPage)
        {
            IWebElement elementToFind = MainPage.Navigate(Driver).GoToSubPage(subPage).Element;
            Assert.IsTrue(elementToFind.Displayed);
        }

        static IEnumerable<SubPageType> GetAllSubPages()
        {
            foreach (var item in Enum.GetValues(typeof(SubPageType)))
            {
                yield return (SubPageType)item;
            }
        }
    }
}
